const { BotQueries } = require('../utilities/mongoClient');

module.exports = app => {
	app.get('/getBotsStatus', async (req, res) => {
		const result = await BotQueries.getBotsStatus();
		res.json(result);
	});
};
