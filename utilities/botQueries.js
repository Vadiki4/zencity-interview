class BotQueries {
	constructor(db) {
		this.collection = db.collection('bots');
	}

	async getBotsStatus() {
		try {
			const res = await this.collection
				.find({
					expertise: { $in: ['data', 'plans', 'services', 'privacy', 'complaints', 'misc'] },
				})
				.toArray();
			return res;
		} catch (err) {
			console.error(err);
		}
	}

	async setBusyBot(name) {
		try {
			const res = await this.collection.updateOne(
				{ expertise: name },
				{ $set: { status: 'Engaged' } },
			);
			return res;
		} catch (err) {
			console.error(err);
		}
	}

	async setFreeBot(name) {
		try {
			const res = await this.collection.updateOne(
				{ expertise: name },
				{ $set: { status: 'Free' } },
			);
			return res;
		} catch (err) {
			console.error(err);
		}
	}

	async getBotAnswer(name) {
		try {
			const res = await this.collection.find({ expertise: name }).toArray();
			return res[0];
		} catch (err) {
			console.error(err);
		}
	}
}
module.exports = BotQueries;
