module.exports = {
	speeches: {
		start:
			'\n\n/engage - start conversation with the bot\n/askforhelp <topic> - Ask a question\n/disengage - Finish the conversation',
		help:
			'\n\nTry using one of my commands:\n\n/engage - start conversation with the bot\n/askforhelp <topic> - Ask a question\n/disengage - Finish the conversation',
	},
	tokens: {
		data: '923157860:AAFPB1Z99kkP6SK7RKXcLvAlpTzbEADRnjY',
		plans: '856402169:AAF7cnGX_3Vg6UoKMKcujkD7GvBt8fyNImw',
		services: '987742589:AAHz5xd1hoA4YfaGUJbVNuLHE7JlBXRvo7Q',
		privacy: '643304341:AAH8hDYW1R-qJu7souikoGHOmzgDMFkCqhM',
		complaints: '930581410:AAEhTaumlP662ZV05Nd55tF0-0WNTRMwtXI',
		misc: '853682525:AAFuqB3BiloktPaxTySZxWw-ioG-mcniz1U',
	},
};
