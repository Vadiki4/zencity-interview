const { MongoClient } = require('mongodb');
const config = require('./config');
const BotQueries = require('./botQueries');

class MongoBot {
	constructor() {
		const url = config.host;
		this.client = new MongoClient(url, config.options);
	}
	async init() {
		await this.client.connect();

		this.db = this.client.db(config.db);
		this.BotQueries = new BotQueries(this.db);
	}
}

module.exports = new MongoBot();
