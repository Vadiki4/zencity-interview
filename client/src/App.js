import React, { useState, useEffect } from 'react';
import Dashboard from './Screens/Dashboard';
import Api from './Utilities/Api';

function App() {
	const [bots, setBots] = useState([]);
	const botSchedule = setInterval(async () => {
		const response = await Api.bots().getBotsStatus();
		if (response.status === 200) {
			setBots(response.data);
		}
	}, 5000);

	useEffect(() => {
		return () => {
			clearInterval(botSchedule);
		};
	});

	return (
		<div className="App">
			<Dashboard bots={bots} />
		</div>
	);
}

export default App;
