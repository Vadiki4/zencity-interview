import axios from 'axios';
const API_ENDPOINT = 'http://localhost:5000';

export default {
	bots() {
		const url = `${API_ENDPOINT}/getBotsStatus`;
		return {
			getBotsStatus: () => {
				return axios.get(url);
			},
		};
	},
};
