import React from 'react';

const Dashboard = ({ bots }) => {
	return (
		<div className="container" style={{ height: window.innerHeight }}>
			<div className="dashboard">
				<div className="names">
					{bots.map((bot, index) => (
						<span key={bot._id}>{`Bot ${index + 1}`}</span>
					))}
				</div>
				<div className="status">
					{bots.map((bot, index) => (
						<span className={bot.status === 'Engaged' ? 'busy' : 'free'} key={bot._id}>
							{bot.status}
						</span>
					))}
				</div>
			</div>
		</div>
	);
};

export default Dashboard;
