const express = require('express');
const cors = require('cors');
const mongoUtil = require('./utilities/mongoClient');
const app = express();
app.use(cors());
const PORT = process.env.PORT || 5000;

async function start() {
	app.listen(PORT);
	console.log('Bot Framework is running!');
	await mongoUtil.init();
	require('./botManager')(app);
	require('./routes/clients')(app);
}

start();
