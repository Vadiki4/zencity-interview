const Telegraf = require('telegraf');
const { BotQueries } = require('../utilities/mongoClient');
const { tokens, speeches } = require('../utilities/botConfig');

module.exports = async () => {
	const botName = 'services';
	const bot = new Telegraf(tokens[botName]);
	const welcomeMessage = 'Hi, my name is Godfather - Services Bot. How can I help?';
	bot.catch(err => console.log('Ooops', err));
	bot.start(ctx => ctx.reply(`${welcomeMessage}${speeches['start']}`));
	bot.help(ctx => ctx.reply(`${welcomeMessage}${speeches['help']}`));
	bot.on('message', async ctx => {
		try {
			if (ctx.message.text.trim() === '/engage') {
				ctx.reply('Im at your service!');
				BotQueries.setBusyBot(botName);
			} else if (ctx.message.text.trim() === '/disengage') {
				ctx.reply('Hope you got everything you need!');
				BotQueries.setFreeBot(botName);
			} else if (ctx.message.text.indexOf('/askforhelp') >= 0) {
				const response = await BotQueries.getBotAnswer(botName);
				ctx.reply(response.answer);
			} else {
				ctx.reply(speeches['help']);
			}
		} catch (err) {
			console.error(err);
		}
	});
	bot.launch();
};
