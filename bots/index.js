module.exports = {
	data: require('./data'),
	plans: require('./plans'),
	services: require('./services'),
	privacy: require('./privacy'),
	complaints: require('./complaints'),
	misc: require('./misc'),
};
